[![A coyote in Yellowstone National Park.](https://gitlab.com/hesyifei/hesyifei/-/raw/main/20191218A-IMGP5724-coyote.jpg "Coyote, Yellowstone National Park, 2019-12-18")](https://photography.hesyifei.com/)

<div align="center">
<a href="https://hesyifei.com/" title="Yifei’s Website" target="_blank">hesyifei.com</a> (<a href="https://yhe.me/" title="Yifei’s Website (Short URL)" target="_blank">yhe.me</a>) / <a href="https://何一非.com/" title="何一非个人网站" target="_blank">何一非.com</a>（<a href="https://非.中国/" title="何一非个人网站（短链接）" target="_blank">非.中国</a>）
</div>
